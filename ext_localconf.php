<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}



\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'UUA.' . $_EXTKEY,
	'Pi1',
	array(
		'Appointment' => 'list, takeVote',
		
	),
	// non-cacheable actions
	array(
		'Appointment' => 'list, takeVote',
		
	)
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'UUA.' . $_EXTKEY,
	'Pi2',
	array(
		'Game' => 'list, create, updateVotes',
		
	),
	// non-cacheable actions
	array(
		'Game' => 'list, create, updateVotes',
		
	)
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'UUA.' . $_EXTKEY,
	'Pi3',
	array(
		'Topic' => 'list, create, new, edit, updateVotes, prepareToEnterTopic',
		'Post' => 'list, show, create',
		
	),
	// non-cacheable actions
	array(
		'Topic' => 'list, create, new, edit, updateVotes, prepareToEnterTopic',
		'Post' => 'list, show, create',
		
	)
);


\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
		'UUA.' . $_EXTKEY,
		'Pi4',
		array(
				'Profile' => 'edit, update',

		),
		// non-cacheable actions
		array(
				'Profile' => 'edit, update',
		)
);
?>