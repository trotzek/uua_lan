<?php
namespace UUA\Lan\Controller;

use TYPO3\CMS\Core\Messaging\FlashMessage;
use UUA\Lan\Domain\Model\Game;
use UUA\Lan\Domain\Model\LanParty;
use UUA\Lan\Domain\Model\Participant;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package lan
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class GameController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	/**
	 * gameRepository
	 *
	 * @var \UUA\Lan\Domain\Repository\GameRepository 
	 * @inject
	 */
	protected $gameRepository;
	
	/**
	 * participantRepository
	 *
	 * @var \UUA\Lan\Domain\Repository\ParticipantRepository 
	 * @inject
	 */
	protected $participantRepository;

    /**
     * @var \UUA\Lan\Domain\Repository\LanPartyRepository 
     * @inject
     */
    protected $lanPartyRepository;

	/**
	 * action list
	 */
	public function listAction() {
		$fe_user = $GLOBALS['TSFE']->fe_user->user;
		$fe_user = $this->participantRepository->findByUid($fe_user['uid']);
		$games = $this->gameRepository->findByLanParty($this->settings['activeLanParty']);
		$this->view->assign('loggedInUser', $fe_user);
		$this->view->assign('games', $games);
	}

	/**
	 * action new
	 *
	 * @param \UUA\Lan\Domain\Model\Game $newGame
	 * @dontvalidate $newGame
	 */
	public function newAction($newGame = NULL) {
		$this->view->assign('newGame', $newGame);
	}

	/**
	 * action create
	 *
	 * @param \UUA\Lan\Domain\Model\Game $newGame
	 */
	public function createAction($newGame) {
		/** @var Participant $participant */
		$participant = $this->participantRepository->findByUid($GLOBALS['TSFE']->fe_user->user['uid']);
		/** @var LanParty $lanParty */
		$lanParty = $this->lanPartyRepository->findByUid(intval($this->settings['activeLanParty']));
		$newGame->addCreator($participant);
        $newGame->setLanParty($lanParty);

		if ($this->gameRepository->containsGame($newGame, $this->settings['activeLanParty'])){
			$this->addFlashMessage("Dieses Spiel ist in der Liste bereits vorhanden", "Fehler", FlashMessage::ERROR);
		}elseif($newGame->getDescription() === ""){
			$this->addFlashMessage("Bitte gib einen Spieletitel an", "Fehler", FlashMessage::ERROR);
		}else{
			$this->gameRepository->add($newGame);
			$this->addFlashMessage('Das Spiel "'.$newGame->getDescription().'" wurde der Liste erfolgreich hinzugefügt.', "Spiel hinzugefügt", FlashMessage::OK);
		}
		$this->redirect('list');
	}

	/**
	 * @param array $owner
	 * @param array $voter
	 * @return void
	 */
	public function updateVotesAction($owner, $voter) {
		$fe_user = $GLOBALS['TSFE']->fe_user->user;
		/** @var Participant $fe_user */
		$fe_user = $this->participantRepository->findByUid($fe_user['uid']);
		$games = $this->gameRepository->findByLanParty(intval($this->settings['activeLanParty']));

		/** @var Game $game */
		foreach ($games as $game){
			if($game->getOwner()->contains($fe_user)){
				$game->removeOwner($fe_user);
			}
			
			if($game->getVoter()->contains($fe_user)){
				$game->removeVoter($fe_user);
			}
			$this->gameRepository->update($game);
		}		
		foreach ($owner as $own){
			$game = $this->gameRepository->findByUid($own);
				
			if($game instanceof Game){
				$game->addOwner($fe_user);
				$this->gameRepository->update($game);
			}
		}
		
		foreach ($voter as $vote){
			$game = $this->gameRepository->findByUid($vote);
			if($game instanceof Game){
				$game->addVoter($fe_user);
				$this->gameRepository->update($game);
			}
		}
		$this->addFlashMessage("Du hast Deine Stimme erfolgreich abgegeben. Du kannst Sie innerhalb der Abstimmungsphase jederzeit ändern.", "Erfolg");
		$this->redirect('list');
	}

}
?>