<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace UUA\Lan\Controller;

use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Saltedpasswords\Salt\SaltFactory;
use UUA\Lan\Domain\Model\Participant;

/**
 *
 *
 * @package lan
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class ProfileController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {
	/**
	 * @var \UUA\Lan\Domain\Repository\ParticipantRepository 
	 * @inject
	 */
	protected $profileRepository;

	public function editAction() {
		$feuser = $this->profileRepository->findByUid($GLOBALS['TSFE']->fe_user->user['uid']);
		$this->view->assign("user", $feuser);
	}

	/**
	 * @param string $oldpassword
	 * @param string $newpassword
	 * @param string $newpasswordrepeat
	 */
	public function updateAction($oldpassword, $newpassword, $newpasswordrepeat){
		$validated = TRUE;
		/** @var Participant $feuser */
		$feuser = $this->profileRepository->findByUid($GLOBALS['TSFE']->fe_user->user['uid']);

		$saltInstance = SaltFactory::getSaltingInstance($feuser->getPassword());

		if (trim($oldpassword) === "" OR trim($newpassword) === "" OR trim($newpasswordrepeat) === ""){
			$this->addFlashMessage("Es müssen alle Felder ausgefüllt werden", "Fehler", FlashMessage::ERROR);
			$validated = FALSE;
		}else{
			$oldPassMatches = $saltInstance->checkPassword($oldpassword, $feuser->getPassword());
			$newPassMatches = ($newpassword == $newpasswordrepeat);
			$newPassLengthTooShort = (strlen($newpassword) < 6 || strlen($newpasswordrepeat) < 6);

			if (!$oldPassMatches){
				$this->addFlashMessage("Das angebenene 'Alte Passwort' ist nicht korrekt!", "Fehler", FlashMessage::ERROR);
				$validated = FALSE;
			}
			if (!$newPassMatches){
				$this->addFlashMessage("Die Felder 'Neues Passwort' und 'Neues Passwort Wiederholen' müssen übereinstimmen", "Fehler", FlashMessage::ERROR);
				$validated = FALSE;
			}
			if ($newPassLengthTooShort){
				$this->addFlashMessage("Das neue Passwort muss mindestens 6 Zeichen lang sein", "Fehler", FlashMessage::ERROR);
				$validated = FALSE;
			}
		}

		if ($validated){
			$newPass = $saltInstance->getHashedPassword($newpassword);
			$feuser->setPassword($newPass);
			$this->profileRepository->update($feuser);

			$this->addFlashMessage('Das Passwort wurde erfolgreich geändert.', "Passwort geändert", FlashMessage::OK);
		}

		$this->redirect("edit");
	}
}