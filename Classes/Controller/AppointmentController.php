<?php
namespace UUA\Lan\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use UUA\Lan\Domain\Model\Appointment;
use UUA\Lan\Domain\Model\Participant;

/**
 *
 *
 * @package lan
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class AppointmentController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	/**
	 * appointmentRepository
	 *
	 * @var \UUA\Lan\Domain\Repository\AppointmentRepository 
	 * @inject
	 */
	protected $appointmentRepository;
	
	/**
	 * @var \UUA\Lan\Domain\Repository\ParticipantRepository 
	 * @inject
	 */
	protected $participantRepository;

	/**
	 * action list
	 */
	public function listAction() {
        $appointments = $this->appointmentRepository->findByLanParty($this->settings['activeLanParty']);
		$participants = $this->participantRepository->findAll();
		$allVotedParticipants = $this->getAllVotedParticipants($appointments);

		$fe_user = $GLOBALS['TSFE']->fe_user->user;
		$fe_user = $this->participantRepository->findByUid($fe_user['uid']);

		$this->view->assign('votedParticipants', $allVotedParticipants);
		$this->view->assign('appointments', $appointments);
		$this->view->assign('loggedInUser', $fe_user);
		$this->view->assign('participants', $participants);

	}

	/**
	 * action takeVote
	 *
	 * @param array $votes
	 */
	public function takeVoteAction($votes) {
		$fe_user = $GLOBALS['TSFE']->fe_user->user;
		/** @var Participant $fe_user */
		$fe_user = $this->participantRepository->findByUid($fe_user['uid']);
		
		$appointments = $this->appointmentRepository->findByLanParty($this->settings['activeLanParty']);
		
		foreach ($appointments as $appointment){
			if(in_array($fe_user, $appointment->getParticipant()->toArray())){
				$appointment->removeParticipant($fe_user);
				$this->appointmentRepository->update($appointment);
			}
		}
		
		foreach ($votes as $vote){
			$appointment = $this->appointmentRepository->findByUid($vote);
			
			if($appointment instanceof Appointment){
				$appointment->addParticipant($fe_user);
				$this->appointmentRepository->update($appointment);
			}
		}
		
		$this->redirect('list');
	}

	/**
	 * Returns all Participants of all appointments who have voted
	 * @param Appointment[] $appointments
	 * @return array
	 */
	private function getAllVotedParticipants($appointments){
		$allVotedParticipants = [];
		foreach ($appointments as $a){
			$allVotedParticipants = $allVotedParticipants + $a->getParticipantsAsArray();
		}
		return $allVotedParticipants;
	}
}
?>