<?php
namespace UUA\Lan\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use UUA\Lan\Domain\Model\Participant;
use UUA\Lan\Domain\Model\Post;
use UUA\Lan\Helper\EmailPumpgun;

/**
 *
 *
 * @package lan
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class PostController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	/**
	 * postRepository
	 *
	 * @var \UUA\Lan\Domain\Repository\PostRepository 
	 * @inject
	 */
	protected $postRepository;

	/**
	 * @var \UUA\Lan\Domain\Repository\ParticipantRepository 
	 * @inject
	 */
	protected $participantRepository;

	/**
	 * @var \UUA\Lan\Domain\Repository\TopicRepository 
	 * @inject
	 */
	protected $topicRepository;

	/**
	 * action list
	 *
	 * @param \UUA\Lan\Domain\Model\Topic $topic
	 */
	public function listAction($topic) {
		$posts = $this->postRepository->findByTopic($topic);
		$newPost = GeneralUtility::makeInstance(Post::class);

		$this->view->assign('newPost', $newPost);
		$this->view->assign('posts', $posts);
		$this->view->assign('topic', $topic);
	}

	/**
	 * @param \UUA\Lan\Domain\Model\Post $newPost
	 * @param \UUA\Lan\Domain\Model\Topic $topic
	 */
	public function createAction($newPost, $topic) {

		$feUser = $GLOBALS['TSFE']->fe_user->user;
		/** @var Participant $feUser */
		$feUser = $this->participantRepository->findByUid($feUser['uid']);

		$newPost->setCreator($feUser);
		$newPost->setCreationDate(new \DateTime());

		if($this->postRepository->isAlreadyPosted($newPost->getText())){
			$this->addFlashMessage("Ein Beitrag mit diesem Text existiert bereits.", "Fehler", FlashMessage::ERROR);
		}elseif($newPost->getText() == ''){
			$this->addFlashMessage("Leere Beiträge sind nicht erlaubt.", "Fehler", FlashMessage::ERROR);
		}else{
			$topic->addPost($newPost);

			$emailPumpgun = GeneralUtility::makeInstance(EmailPumpgun::class);

			if($emailPumpgun instanceof EmailPumpgun){

				$participants = $this->participantRepository->findAll();

				/** @var Participant $participant */
				foreach ($participants as $participant){
					if(!($topic->getEmailPumpgunBlacklist()->contains($participant)) && !($participant->getEmail() == '') && !($participant === $feUser)){
						$template = $emailPumpgun->getNewPostTemplate();
						$template->assign('participant', $participant);
						$template->assign('topic', $topic);

						$rounds = array();
						$rounds['target'] = $participant->getEmail();
						$rounds['sender'] = 'noreply@lanplanung.de';
						$rounds['subject'] = '[LAN-Planung] Neuer Beitrag';
						$rounds['message'] = $template->render();
						$emailPumpgun->fire($rounds);
					}
					$topic->addEmailPumpgunBlacklist($participant);
				}
			}
			$this->topicRepository->update($topic);
			$topic->removeEmailPumpgunBlacklist($feUser);
		}

		$this->redirect('list', 'Post', $this->extensionName, array('topic' => $topic));
	}

}
?>