<?php

	namespace UUA\Lan\Helper;


	class EmailPumpgun {

		/**
		 * loads an Standalone Template for the Pumpgun
		 *
		 * @return \TYPO3\CMS\Fluid\View\StandaloneView
		 */
		public function getNewPostTemplate(){
			$emailTemplate = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Fluid\View\StandaloneView::class);
			$emailTemplate->setTemplatePathAndFilename(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('lan').'Resources/Private/Templates/Email/NewPost.html');

			return $emailTemplate;
		}

		/**
		 * @param \array $rounds
		 */
		public function fire($rounds){
			$mail = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Mail\MailMessage::class);
			$mail->setTo($rounds['target'])
				 ->setFrom($rounds['sender'])
				 ->setSubject($rounds['subject'])
				 ->setBody($rounds['message'], 'text/plain');
			$mail->send();
		}

	}

?>