<?php
namespace UUA\Lan\ViewHelpers;

	use Iterator;

	class InArrayViewHelper  extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {
		
		/**
		 * @param mixed $haystack
		 * @param mixed $needle
		 * @return string
		 */
		public function render($haystack, $needle){
			
			if($haystack instanceof Iterator){
				foreach ($haystack as $hayneedle){
					if($hayneedle == $needle){
						return '1';
					}
				}
				
			}
			
		}
		
	}


?>