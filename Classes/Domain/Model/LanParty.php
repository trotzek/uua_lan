<?php
namespace UUA\Lan\Domain\Model;

    /***************************************************************
     *  Copyright notice
     *
     *  (c) 2013
     *  All rights reserved
     *
     *  This script is part of the TYPO3 project. The TYPO3 project is
     *  free software; you can redistribute it and/or modify
     *  it under the terms of the GNU General Public License as published by
     *  the Free Software Foundation; either version 3 of the License, or
     *  (at your option) any later version.
     *
     *  The GNU General Public License can be found at
     *  http://www.gnu.org/copyleft/gpl.html.
     *
     *  This script is distributed in the hope that it will be useful,
     *  but WITHOUT ANY WARRANTY; without even the implied warranty of
     *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *  GNU General Public License for more details.
     *
     *  This copyright notice MUST APPEAR in all copies of the script!
     ***************************************************************/
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

/**
 *
 *
 * @package lan
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class LanParty extends AbstractEntity {

    /**
     * @var \string 
     */
    protected $description;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\UUA\Lan\Domain\Model\Appointment> 
     */
    protected $appointments;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\UUA\Lan\Domain\Model\Game> 
     */
    protected $games;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\UUA\Lan\Domain\Model\Topic> 
     */
    protected $topics;

    /**
     * __construct
     */
    public function __construct() {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties.
     *
     * @return void
     */
    protected function initStorageObjects() {
        /**
         * Do not modify this method!
         * It will be rewritten on each save in the extension builder
         * You may modify the constructor of this class instead
         */
        $this->appointments = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->games = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->topics = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $appointments
     */
    public function setAppointments($appointments)
    {
        $this->appointments = $appointments;
    }

    /**
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage
     */
    public function getAppointments()
    {
        return $this->appointments;
    }

    /**
     * Adds an Appointment
     *
     * @param \UUA\Lan\Domain\Model\Appointment $appointment
     */
    public function addAppointment(\UUA\Lan\Domain\Model\Appointment $appointment) {
        $this->appointments->attach($appointment);
    }

    /**
     * Removes an Appointment
     *
     * @param \UUA\Lan\Domain\Model\Appointment $appointmentToRemove
     * @return void
     */
    public function removeAppointment(\UUA\Lan\Domain\Model\Appointment $appointmentToRemove) {
        $this->appointments->detach($appointmentToRemove);
    }

    /**
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $games
     */
    public function setGames($games)
    {
        $this->games = $games;
    }

    /**
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage
     */
    public function getGames()
    {
        return $this->games;
    }

    /**
     * Adds a Game
     *
     * @param \UUA\Lan\Domain\Model\Game $game
     */
    public function addGame(\UUA\Lan\Domain\Model\Game $game) {
        $this->games->attach($game);
    }

    /**
     * Removes a Game
     *
     * @param \UUA\Lan\Domain\Model\Game $gameToRemove
     */
    public function removeGame(\UUA\Lan\Domain\Model\Game $gameToRemove) {
        $this->games->detach($gameToRemove);
    }

    /**
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $topics
     */
    public function setTopics($topics)
    {
        $this->topics = $topics;
    }

    /**
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage
     */
    public function getTopics()
    {
        return $this->topics;
    }

    /**
     * Adds a Topic
     *
     * @param \UUA\Lan\Domain\Model\Topic $topic
     */
    public function addTopic(\UUA\Lan\Domain\Model\Topic $topic) {
        $this->topics->attach($topic);
    }

    /**
     * Removes a Topic
     *
     * @param \UUA\Lan\Domain\Model\Topic $topicToRemove
     */
    public function removeTopic(\UUA\Lan\Domain\Model\Topic $topicToRemove) {
        $this->topics->detach($topicToRemove);
    }

}