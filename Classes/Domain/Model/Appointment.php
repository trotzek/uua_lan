<?php
namespace UUA\Lan\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package lan
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Appointment extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * appointmentDate
	 *
	 * @var \DateTime 
	 */
	protected $appointmentDate;

	/**
	 * participant
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\UUA\Lan\Domain\Model\Participant> 
	 */
	protected $participant;

    /**
     * @var \UUA\Lan\Domain\Model\LanParty 
     */
    protected $lanParty;

	/**
	 * __construct
	 *
	 * @return Appointment
	 */
	public function __construct() {
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all ObjectStorage properties.
	 *
	 * @return void
	 */
	protected function initStorageObjects() {
		/**
		 * Do not modify this method!
		 * It will be rewritten on each save in the extension builder
		 * You may modify the constructor of this class instead
		 */
		$this->participant = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
	}

	/**
	 * Returns the appointmentDate
	 *
	 * @return \DateTime $appointmentDate
	 */
	public function getAppointmentDate() {
		return $this->appointmentDate;
	}

	/**
	 * Sets the appointmentDate
	 *
	 * @param \DateTime $appointmentDate
	 * @return void
	 */
	public function setAppointmentDate($appointmentDate) {
		$this->appointmentDate = $appointmentDate;
	}

	/**
	 * Adds a Participant
	 *
	 * @param \UUA\Lan\Domain\Model\Participant $participant
	 * @return void
	 */
	public function addParticipant(\UUA\Lan\Domain\Model\Participant $participant) {
		$this->participant->attach($participant);
	}

	/**
	 * Removes a Participant
	 *
	 * @param \UUA\Lan\Domain\Model\Participant $participantToRemove The Participant to be removed
	 * @return void
	 */
	public function removeParticipant(\UUA\Lan\Domain\Model\Participant $participantToRemove) {
		$this->participant->detach($participantToRemove);
	}

	/**
	 * Returns the participant
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\UUA\Lan\Domain\Model\Participant> $participant
	 */
	public function getParticipant() {
		return $this->participant;
	}

	/**
	 * Sets the participant
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\UUA\Lan\Domain\Model\Participant> $participant
	 * @return void
	 */
	public function setParticipant(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $participant) {
		$this->participant = $participant;
	}

    /**
     * @param \UUA\Lan\Domain\Model\LanParty $lanParty
     */
    public function setLanParty($lanParty) {
        $this->lanParty = $lanParty;
    }

    /**
     * @return \UUA\Lan\Domain\Model\LanParty
     */
    public function getLanParty()
    {
        return $this->lanParty;
    }

	/**
	 * @return int
	 */
	public function getCountOfVoters(){
		return count($this->participant->toArray());
	}

	public function getOverallPercentage(){
		$participantRepository = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\UUA\Lan\Domain\Repository\ParticipantRepository::class);
		$overallParticipants = $participantRepository->findAll();
		$participantCount = count($this->participant);
		$overallCount = count($overallParticipants);
		return 100 / $overallCount * $participantCount;
	}

	/**
	 * Returns all Participants who already voted as an array with the uid as array key which is easy to merge with
	 * other appointments
	 *
	 * @return array
	 */
	public function getParticipantsAsArray(){
		$participantsWhoVoted = [];
		/** @var Participant $p */
		foreach($this->getParticipant() as $p){
			$participantsWhoVoted[$p->getUid()] = $p;
		}
		return $participantsWhoVoted;
	}

	/**
	 * Returns all participants as a comma separated list
	 *
	 * @return string
	 */
	public function getAllParticipantsList(){
		$allParticipants = [];
		/** @var Participant $p */
		foreach ($this->getParticipant() as $p){
			$allParticipants[$p->getUid()] = $p->getFirstName();
		}
		return implode(', ', $allParticipants);
	}
}
?>