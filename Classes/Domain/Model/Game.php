<?php
namespace UUA\Lan\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package lan
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Game extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * description
	 * @required
	 * @var \string 
	 */
	protected $description;

	/**
	 * creator
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\UUA\Lan\Domain\Model\Participant> 
	 */
	protected $creator;

	/**
	 * owner
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\UUA\Lan\Domain\Model\Participant> 
	 */
	protected $owner;
	
	/**
	 * voter
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\UUA\Lan\Domain\Model\Participant> 
	 */
	protected $voter;

    /**
     * @var \UUA\Lan\Domain\Model\LanParty 
     */
    protected $lanParty;

	/**
	 * @var string
	 */
	protected $additionalDescription;

	/**
	 * @var string
	 */
	protected $link;

	/**
	 * __construct
	 *
	 * @return Game
	 */
	public function __construct() {
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all ObjectStorage properties.
	 *
	 * @return void
	 */
	protected function initStorageObjects() {
		/**
		 * Do not modify this method!
		 * It will be rewritten on each save in the extension builder
		 * You may modify the constructor of this class instead
		 */
		$this->creator = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->owner = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->voter = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
	}

	/**
	 * Returns the description
	 *
	 * @return \string $description
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * Sets the description
	 *
	 * @param \string $description
	 * @return void
	 */
	public function setDescription($description) {
		$this->description = $description;
	}

	/**
	 * Adds a Participant
	 *
	 * @param \UUA\Lan\Domain\Model\Participant $creator
	 * @return void
	 */
	public function addCreator(\UUA\Lan\Domain\Model\Participant $creator) {
		$this->creator->attach($creator);
	}

	/**
	 * Removes a Participant
	 *
	 * @param \UUA\Lan\Domain\Model\Participant $creatorToRemove The Participant to be removed
	 * @return void
	 */
	public function removeCreator(\UUA\Lan\Domain\Model\Participant $creatorToRemove) {
		$this->creator->detach($creatorToRemove);
	}

	public function getFirstCreator(){
		$arrCreators = $this->creator->toArray();		
		return $arrCreators[0];
	}
	/**
	 * Returns the creator
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\UUA\Lan\Domain\Model\Participant> $creator
	 */
	public function getCreator() {
		return $this->creator;
	}

	/**
	 * Sets the creator
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\UUA\Lan\Domain\Model\Participant> $creator
	 * @return void
	 */
	public function setCreator(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $creator) {
		$this->creator = $creator;
	}

	/**
	 * Adds a Participant
	 *
	 * @param \UUA\Lan\Domain\Model\Participant $owner
	 * @return void
	 */
	public function addOwner(\UUA\Lan\Domain\Model\Participant $owner) {
		$this->owner->attach($owner);
	}

	/**
	 * Removes a Participant
	 *
	 * @param \UUA\Lan\Domain\Model\Participant $ownerToRemove The Participant to be removed
	 * @return void
	 */
	public function removeOwner(\UUA\Lan\Domain\Model\Participant $ownerToRemove) {
		$this->owner->detach($ownerToRemove);
	}

	/**
	 * Returns the owner
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\UUA\Lan\Domain\Model\Participant> $owner
	 */
	public function getOwner() {
		return $this->owner;
	}

	/**
	 * Sets the owner
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\UUA\Lan\Domain\Model\Participant> $owner
	 * @return void
	 */
	public function setOwner(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $owner) {
		$this->owner = $owner;
	}
	

	/**
	 * Adds a Participant
	 *
	 * @param \UUA\Lan\Domain\Model\Participant $voter
	 * @return void
	 */
	public function addVoter(\UUA\Lan\Domain\Model\Participant $voter) {
		$this->voter->attach($voter);
	}
	
	/**
	 * Removes a Participant
	 *
	 * @param \UUA\Lan\Domain\Model\Participant $voterToRemove The Participant to be removed
	 * @return void
	 */
	public function removeVoter(\UUA\Lan\Domain\Model\Participant $voterToRemove) {
		$this->voter->detach($voterToRemove);
	}
	
	/**
	 * Returns the voter
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\UUA\Lan\Domain\Model\Participant> $voter
	 */
	public function getVoter() {
		return $this->voter;
	}
	
	/**
	 * Sets the voter
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\UUA\Lan\Domain\Model\Participant> $voter
	 * @return void
	 */
	public function setVoter(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $voter) {
		$this->voter = $voter;
	}

    /**
     * @param \UUA\Lan\Domain\Model\LanParty $lanParty
     */
    public function setLanParty($lanParty) {
        $this->lanParty = $lanParty;
    }

    /**
     * @return \UUA\Lan\Domain\Model\LanParty
     */
    public function getLanParty() {
        return $this->lanParty;
    }

	/**
	 * @return string
	 */
	public function getAdditionalDescription() {
		return $this->additionalDescription;
	}

	/**
	 * @param string $additionalDescription
	 */
	public function setAdditionalDescription($additionalDescription) {
		$this->additionalDescription = $additionalDescription;
	}

	/**
	 * @return string
	 */
	public function getLink() {
		return $this->link;
	}

	/**
	 * Returns the link but makes sure the returned link has a protocol (such as http://)
	 * @return string
	 */
	public function getLinkWithProtocol(){
		$urlParts = parse_url($this->getLink());
		if (!isset($urlParts['scheme'])) {
			$completeUrl = "http://".$this->getLink();
		}else{
			$completeUrl = $this->getLink();
		}
		return $completeUrl;
	}

	/**
	 * @param string $link
	 */
	public function setLink($link) {
		$this->link = $link;
	}
}
?>