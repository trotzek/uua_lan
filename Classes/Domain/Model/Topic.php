<?php
namespace UUA\Lan\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package lan
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Topic extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * title
	 *
	 * @var \string 
	 */
	protected $title;

	/**
	 * sort
	 *
	 * @var \string 
	 */
	protected $sort;

	/**
	 * description
	 *
	 * @var \string 
	 */
	protected $description;
	
	/**
	 * posts
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\UUA\Lan\Domain\Model\Post> 
	 */
	protected $posts;

    /**
     * @var \UUA\Lan\Domain\Model\LanParty 
     */
    protected $lanParty;

	/**
	 * emailPumpgunBlacklist
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\UUA\Lan\Domain\Model\Participant> 
	 */
	protected $emailPumpgunBlacklist;

	/**
	 */
	public function __construct() {
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all ObjectStorage properties.
	 */
	protected function initStorageObjects() {
		/**
		 * Do not modify this method!
		 * It will be rewritten on each save in the extension builder
		 * You may modify the constructor of this class instead
		 */
		$this->posts = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		
		$this->emailPumpgunBlacklist = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
	}

	/**
	 * Returns the title
	 *
	 * @return \string $title
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * @param \string $title
	 */
	public function setTitle($title) {
		$this->title = $title;
	}

	/**
	 * @return \string $sort
	 */
	public function getSort() {
		return $this->sort;
	}

	/**
	 * @param \string $sort
	 */
	public function setSort($sort) {
		$this->sort = $sort;
	}

	/**
	 * Returns the description
	 *
	 * @return \string $description
	 */
	public function getDescription() {
		return $this->description;
	}
	
	/**
	 * Sets the description
	 *
	 * @param \string $description
	 * @return void
	 */
	public function setDescription($description) {
		$this->description = $description;
	}
	
	/**
	 * @param \UUA\Lan\Domain\Model\Post $post
	 */
	public function addPost(\UUA\Lan\Domain\Model\Post $post) {
		$this->posts->attach($post);
	}

	/**
	 * Removes a Post
	 *
	 * @param \UUA\Lan\Domain\Model\Post $postToRemove The Post to be removed
	 */
	public function removePost(\UUA\Lan\Domain\Model\Post $postToRemove) {
		$this->posts->detach($postToRemove);
	}

	/**
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\UUA\Lan\Domain\Model\Post> $posts
	 */
	public function getPosts() {
		return $this->posts;
	}

	/**
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\UUA\Lan\Domain\Model\Post> $posts
	 */
	public function setPosts(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $posts) {
		$this->posts = $posts;
	}

	/**
	 * @param \UUA\Lan\Domain\Model\Participant $emailPumpgunBlacklist
	 */
	public function addEmailPumpgunBlacklist(\UUA\Lan\Domain\Model\Participant $emailPumpgunBlacklist) {
		$this->emailPumpgunBlacklist->attach($emailPumpgunBlacklist);
	}

	/**
	 * @param \UUA\Lan\Domain\Model\Participant $emailPumpgunBlacklistToRemove The Participant to be removed
	 */
	public function removeEmailPumpgunBlacklist(\UUA\Lan\Domain\Model\Participant $emailPumpgunBlacklistToRemove) {
		$this->emailPumpgunBlacklist->detach($emailPumpgunBlacklistToRemove);
	}

	/**
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\UUA\Lan\Domain\Model\Participant> $emailPumpgunBlacklist
	 */
	public function getEmailPumpgunBlacklist() {
		return $this->emailPumpgunBlacklist;
	}

	/**
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\UUA\Lan\Domain\Model\Participant> $emailPumpgunBlacklist
	 */
	public function setEmailPumpgunBlacklist(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $emailPumpgunBlacklist) {
		$this->emailPumpgunBlacklist = $emailPumpgunBlacklist;
	}

    /**
     * @param \UUA\Lan\Domain\Model\LanParty $lanParty
     */
    public function setLanParty($lanParty) {
        $this->lanParty = $lanParty;
    }

    /**
     * @return \UUA\Lan\Domain\Model\LanParty
     */
    public function getLanParty() {
        return $this->lanParty;
    }

	/**
	 * @return \int
	 */
	public function getPostsCount(){
		return count($this->posts);
	}
	
	/**
	 * @return \UUA\Lan\Domain\Model\Post
	 */
	public function getFirstPost(){
		$arrayedPosts = $this->posts->toArray();
		
		if(count($arrayedPosts) > 0){
			return $arrayedPosts[0];
		}else{
			return null;
		}
		
	}
	
	/**
	 * @return \UUA\Lan\Domain\Model\Post
	 */
	public function getLastPost(){
		$arrayedPosts = $this->posts->toArray();
	
		if(count($arrayedPosts) > 0){
			return $arrayedPosts[count($arrayedPosts)-1];
		}else{
			return null;
		}
	
	}

}
?>