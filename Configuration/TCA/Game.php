<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$TCA['tx_lan_domain_model_game'] = array(
	'ctrl' => $TCA['tx_lan_domain_model_game']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, description, additional_description, creator, owner, voter, lan_party',
	),
	'types' => array(
		'1' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, description, link, additional_description, creator, owner, voter, lan_party,--div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access,starttime, endtime'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_lan_domain_model_game',
				'foreign_table_where' => 'AND tx_lan_domain_model_game.pid=###CURRENT_PID### AND tx_lan_domain_model_game.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'description' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:lan/Resources/Private/Language/locallang_db.xlf:tx_lan_domain_model_game.description',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
        'lan_party' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:lan/Resources/Private/Language/locallang_db.xlf:tx_lan_domain_model_game.lan_party',
            'config' => array(
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_lan_domain_model_lanparty',
                'maxitems' => 1,
                'minitems' => 1,
                'multiple' => 0,
            ),
        ),
		'creator' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:lan/Resources/Private/Language/locallang_db.xlf:tx_lan_domain_model_game.creator',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'foreign_table' => 'fe_users',
				'MM' => 'tx_lan_game_participant_mm',
				'maxitems' => 9999,
				'multiple' => 0,
			),
		),
		'owner' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:lan/Resources/Private/Language/locallang_db.xlf:tx_lan_domain_model_game.owner',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingleBox',
				'foreign_table' => 'fe_users',
				'MM' => 'tx_lan_game_owner_participant_mm',
				'maxitems' => 9999,
				'multiple' => 0,
			),
		),
		'voter' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:lan/Resources/Private/Language/locallang_db.xlf:tx_lan_domain_model_game.voter',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingleBox',
				'foreign_table' => 'fe_users',
				'MM' => 'tx_lan_game_voter_participant_mm',
				'size' => 10,
				'autoSizeMax' => 30,
				'maxitems' => 9999,
				'multiple' => 0,
			),
		),
		'link' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:lan/Resources/Private/Language/locallang_db.xlf:tx_lan_domain_model_game.link',
			'config' => array(
				'type' => 'input',
				'size' => 100,
				'eval' => 'trim'
			),
		),
		'additional_description' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:lan/Resources/Private/Language/locallang_db.xlf:tx_lan_domain_model_game.additional_description',
			'config' => array(
				'type' => 'text',
				'rows' => 10,
				'cols' => 100,
			),
		),
	),
);

?>