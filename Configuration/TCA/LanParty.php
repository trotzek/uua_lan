<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$TCA['tx_lan_domain_model_lanparty'] = array(
	'ctrl' => $TCA['tx_lan_domain_model_lanparty']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, description, appointments, games, topics',
	),
	'types' => array(
		'1' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, description, appointments, games, topics,--div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access,starttime, endtime'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_lan_domain_model_lanparty',
				'foreign_table_where' => 'AND tx_lan_domain_model_lanparty.pid=###CURRENT_PID### AND tx_lan_domain_model_lanparty.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'description' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:lan/Resources/Private/Language/locallang_db.xlf:tx_lan_domain_model_lanparty.description',
			'config' => array(
				'type' => 'input',
				'size' => 10,
				'eval' => 'trim',
			),
		),
		'appointments' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:lan/Resources/Private/Language/locallang_db.xlf:tx_lan_domain_model_lanparty.appointments',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_lan_domain_model_appointment',
                'foreign_field' => 'lan_party',
				'size' => 10,
				'autoSizeMax' => 30,
				'maxitems' => 9999,
				'multiple' => 0,
			),
		),
        'games' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:lan/Resources/Private/Language/locallang_db.xlf:tx_lan_domain_model_lanparty.games',
            'config' => array(
                'type' => 'inline',
                'foreign_table' => 'tx_lan_domain_model_game',
                'foreign_field' => 'lan_party',
                'size' => 10,
                'autoSizeMax' => 30,
                'maxitems' => 9999,
                'multiple' => 0,
            ),
        ),
        'topics' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:lan/Resources/Private/Language/locallang_db.xlf:tx_lan_domain_model_lanparty.topics',
            'config' => array(
                'type' => 'inline',
                'foreign_table' => 'tx_lan_domain_model_topic',
                'foreign_field' => 'lan_party',
                'size' => 10,
                'autoSizeMax' => 30,
                'maxitems' => 9999,
                'multiple' => 0,
            ),
        ),
	),
);

?>